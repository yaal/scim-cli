scim-cli
========

scim-cli is a tool to help you develop scim applications.

.. _cli:

.. click:: scim_cli:cli
   :prog: scim
   :nested: full
